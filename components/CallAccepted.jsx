import * as React from "react";

import {
    SettingsIcon,
    FullScreen,
    VideoOffIcon,
    VideoOnIcon,
    PhoneIcon,
    MicIcon,
    MicOffIcon,
    ToggleLargeIcon,
  } from "./icons";

import { RTC_Style } from "../style.js";
import CallContext from "../context/context";
import {useContext, useRef, useEffect} from "react"

const CallAccepted = (props) => {
    const {videoPoster, handleCallHangup} = props 
    const {callState, stopRTC} = useContext(CallContext)

    // https://stackoverflow.com/questions/53561913/react-forwarding-multiple-refs
    // const {remoteVideo, localVideo} = ref
    const localVideo = useRef()
    const remoteVideo = useRef()

    useEffect(()=>{
        remoteVideo.current.srcObject = callState.RTC_Call.remoteStream 
        localVideo.current.srcObject = callState.RTC_Call.localStream 
    }, [callState.RTC_Call.localStream, callState.RTC_Call.remoteStream]) 

    return (
    <div style={{ position: "relative", margin: "auto", width: "100%" }}>
        <div
            style={RTC_Style.videoContainer}
        >
            
            <video
                style={RTC_Style.mainVidStyles}
                autoPlay
                ref={remoteVideo}
                poster={videoPoster}
            />
            {/* <div style={RTC_Style.secondaryVideoContainer}> */}
                <video
                    style={RTC_Style.secondaryVidStyles}
                    autoPlay
                    muted
                    vol={0}
                    ref={localVideo}
                />
            {/* </div> */}
        </div>
        {/* <div className="video-action-btns"> */}
        
        <div style={RTC_Style.videoActionBtnGroup}>
            <button style={RTC_Style.Btn}>
                <SettingsIcon />
            </button>
            <button style={RTC_Style.Btn}>
                <VideoOnIcon />
            </button>
            <button style={RTC_Style.Btn} onClick={stopRTC}>
                <PhoneIcon />
            </button>
            <button style={RTC_Style.Btn}>
                <MicIcon />
            </button>
            <button style={RTC_Style.Btn}>
                <ToggleLargeIcon />
            </button>
        </div>
    </div>
  )
}

export default CallAccepted