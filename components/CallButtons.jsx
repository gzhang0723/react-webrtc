import * as React from "react";
import CallContext from "../context/context";
import {useContext} from "react"
import { Call } from "../AppRTCJS/call";
//import "../public/style.css" 

const CallButton = ({buttonText, joinExisting, roomId}) => {

    console.log("CallButton props are ", buttonText, joinExisting, roomId)

    const {startRTC} = useContext(CallContext)

    return ( <div className="grid grid-cols-2 gap-2 p-2 m-1 justify-center"> 
                <button className="flex justify-center p-1 text-white items-center bg-cyan-400  hover:bg-red-400" onClick={startRTC}> 
                    <svg className="stroke-2 w-4 p-1" xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                        <path strokeLinecap="round" strokeLinejoin="round" d="M15 10l4.553-2.276A1 1 0 0121 8.618v6.764a1 1 0 01-1.447.894L15 14M5 18h8a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v8a2 2 0 002 2z" />
                    </svg>
                </button>
                <button className="flex justify-center p-1 text-white items-center bg-cyan-400 hover:bg-red-400" onClick={startRTC}> 
                    <svg className="stroke-2 w-4 p-1" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M19 11a7 7 0 01-7 7m0 0a7 7 0 01-7-7m7 7v4m0 0H8m4 0h4m-4-8a3 3 0 01-3-3V5a3 3 0 116 0v6a3 3 0 01-3 3z" />
                    </svg>
                    
                </button>
            </div> );
}
 
export default CallButton;