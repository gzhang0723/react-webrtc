import * as React from 'react';
import SettingsIcon from "../icons/SettingsIcon";

const SettingsButton = props => {
    return (<a className={"acquire-tooltip-wrapper video-setting-btn"} disabled={props.disabled}>
                <SettingsIcon />
                <div className="acquire-tooltip-content acquire-tooltip-content-top right-pos">Setting</div>
            </a>);
}
export default SettingsButton;