import * as React from 'react';
import MicIcon from '../icons/MicIcon';
import MicOffIcon from '../icons/MicOffIcon';

const ToggleAudioButton = props => {

    const { isAudioEnabled, hasAudioTrack, disabled} = props

    return (<a className={"acquire-tooltip-wrapper audio"}  disabled={!hasAudioTrack || disabled}>
                {isAudioEnabled ? <MicIcon /> : <MicOffIcon />}
                <div className="acquire-tooltip-content acquire-tooltip-content-top left-pos">{!hasAudioTrack ? 'No Audio' : isAudioEnabled ? 'Mute' : 'Unmute'}</div>
            </a>);
}
export default ToggleAudioButton