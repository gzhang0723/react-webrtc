export { default as AvatarIcon } from './AvatarIcon';
export { default as ErrorIcon } from './ErrorIcon';
export { default as FullScreen } from './FullScreen';
export { default as MicIcon } from './MicIcon';
export { default as MicOffIcon } from './MicOffIcon';
export { default as MoreIcon } from './MoreIcon';
export { default as PhoneIcon } from "./PhoneIcon";
export { default as SettingsIcon } from './SettingsIcon';
export { default as SuccessIcon } from "./SuccessIcon";
export { default as TIcon } from "./TIcon";
export { default as ToggleLargeIcon } from "./ToggleLargeIcon";
export { default as ToggleSmallIcon } from "./ToggleSmallIcon";
export { default as VideoOffIcon } from "./VideoOffIcon";
export { default as VideoOnIcon } from "./VideoOnIcon";
export { default as WarningIcon } from "./WarningIcon";

// import WarningIcon from "./WarningIcon";

// export {WarningIcon} 

