import React from "react";

const VisitorIcon = () => {
    return (
        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox="0 0 128 128">
            <style>
            </style>
            <defs>
                <linearGradient id="linear-gradient" x1="6.53" y1="42.2" x2="141.87" y2="93.54" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#4ec0e2" />
                    <stop offset="1" stop-color="#49cdb3" />
                </linearGradient>
            </defs>
            <title>human icon100</title>
            <rect class="cls-1" width="128" height="128" />
            <path class="cls-2" d="M93.13,94.94H34.88a1,1,0,0,1-1-1V84.06c0-.5,1-10.4,23.37-15.06V66.93c-1.83-1.48-9-7.88-8.81-17.26,0-.26-.2-7.54,4.52-12.42,2.69-2.78,6.4-4.19,11-4.19s8.35,1.41,11,4.19c4.72,4.88,4.53,12.16,4.52,12.47.21,9.34-7,15.73-8.81,17.21V69c22.32,4.66,23.34,14.56,23.37,15v10A1,1,0,0,1,93.13,94.94Zm-57.26-2H92.13V84.06s-1.16-9-22.58-13.27a1,1,0,0,1-.8-1V66.44a1,1,0,0,1,.41-.81c.08-.06,8.62-6.45,8.4-15.92,0-.12.16-6.82-4-11.08-2.3-2.37-5.53-3.57-9.6-3.57s-7.3,1.2-9.6,3.57c-4.12,4.26-4,11-4,11-.22,9.54,8.31,15.91,8.4,16a1,1,0,0,1,.41.81v3.37a1,1,0,0,1-.8,1C37,75.05,35.88,84.06,35.87,84.15Z" ></path>
        </svg>
    );
}
export default VisitorIcon;