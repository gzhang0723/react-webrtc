import * as React from "react";
import CallContext from "../context/context";
import {useContext} from "react"
import VisitorLogo from './icons/VisitorPhoto'
import "./CallPending.css"; 
//import ringtone from './ringtone.mp3'; 

const CallingComponent = (props) => {
    const { rejectHandler } = props 
    const {startRTC} = useContext(CallContext)

    const acceptHandler = () => startRTC(false)  // isInitiator = false 
    
    return (
        <div className="ccContainer">
            <h6 className="headerStyle">Incoming Video Call</h6>
            <div className="visitor_photo">
                {/* TODO: we need to serve ringtone.mp3 on our site. Widget side ringtone cannot play because file-loader cannot find src of it. http://localhost/object2module 404 not found */}
                <audio src={require('./ringtone.mp3')} loop autoPlay={true} />

                <audio src={"https://www.zapsplat.com/wp-content/uploads/2015/sound-effects-77317/zapsplat_multimedia_ringtone_smartphone_mallet_musical_002_79296.mp3"} loop autoPlay={true} />
                <div className="launcher-vibrate"></div>
            </div>
            
            <div className="btnContainer">
                <button className="btn accept" onClick={acceptHandler}>Accept</button>
                <button className="btn reject" onClick={rejectHandler}>Reject</button>
            </div>

        </div>
    );
};

export default CallingComponent;