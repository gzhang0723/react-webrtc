/*
 *
 *  PLEASE MINIMIZE changes to the library itself
 *  This module is for media stream only. The customized app logic or UI can be handled in different place
 *  The accompanying testing module is to demo how to use this module and for reference only
 *
 */

/**
 * components props
 *
 *
 * @param {Boolean} debug false will disable video div
 * @param {number} roomId logic seperation for video call groups
 * @param {Boolean} record should this component record the call
 * @param {func} onGetLocalStream local stream external handler
 * @param {func} onGetRemoteStream remote stream external handler
 * @param {Boolean} isStopRecoring the stop recording signaling from external
 * @param {callback->uuid, fileSize, duration} onRecordingReady callback for recording ready, will pass back the uuid of the recording file
 * @param {String} buttonView customized button label
 * @param {Boolean} joinExisting wether or not the starter of the room
 * @param {Boolean} startRTC start RTC externally
 * @param {text} buttonText start RTC externally
 * @param {Boolean} videoEnabled the video constraint enabled signaling from external
 * @param {String} videoPoster the video background url
 *
 *
 * Things can be customized:
 * 1. button view
 * 2. seperate streams to redux store; so button and streams can be seperated
 * 3. support multiple streams and peers [Todo]
 *
 */

import * as React from 'react'
import { useEffect, useRef, useState } from "react";
import { Call } from "./AppRTCJS/call";
import { v4 as uuidv4 } from "uuid";
import { startRecording, stopRecording } from "./services/kurento";
import { getMixStream, CatchAll } from "./services/mixer";
import params from "./context/param";
import adapter from "webrtc-adapter";
import { parseJSON } from "./AppRTCJS/util";
import CallPendingComponent from "./components/CallPending";
import CallAcceptedComponent from "./components/CallAccepted";
import io from "socket.io-client";
import axios from "axios";
import CallStore from './context/state';
import CallApp from './callApp'
import "./public/style.css" 


const WebRTC = (props) => {
  let {
    record,
    roomId,
    joinExisting,
    isStopRecoring,
    onRecordingReady,
    showDisplay,
    buttonText,
    onMixStreamDebug,
    videoEnabled,
    videoPoster,
    extStyle,
    callExtStatus,
    alert
  } = props;
  console.log("props are ", props);

  return (
    <div>
        <CallStore alert={alert} joinExisting={joinExisting} roomId={roomId}> 
          <CallApp {...props} />
        </CallStore>
    </div>
  );
};

export default WebRTC;
