export const RTC_Style = {
  videoContainer: {
    /* width is set as 100% here. any width can be specified as per requirement */
    // pad with proportion to width and this will give space to video element, 75% - 4:3; 56.25% - 16:9 
    width: "100%", 
    paddingTop: "56.25%" ,  
    height: "0px", 
    position: "relative",
    backgroundColor: "blue", 
    // paddingBottom: "5%"
    marginBottom: "10px", 
    marginTop:"10px"
  },
  mainVidStyles: {
    position: "absolute",
    top: "0px",
    left: "0px", 
    width: "100%",
    margin: "0px", 
    height: "100%",
    aspectRatio: 4 / 4,
    backgroundColor: "blue",
  },
  secondaryVidStyles: {
    position: "absolute",
    top: "10px",
    left: "10px",
    width: "30%",
    height: "auto",
    aspectRatio: 4 / 4,
    backgroundColor: "blue",
  },
  videoActionBtnGroup: {
    display: "grid",
    gridTemplateColumns: "auto auto auto auto auto",
    gridTemplateRows: "100%", 
    position: "absolute", 
    width: "100%", 
    bottom : "10px", 
  }, 
  Btn: {
    display: "flex", 
    height: "32px",  
    width: "32px", 
    borderRadius: "18px", 
    backgroundColor: "black", 
    alignSelf: "center", 
    justifySelf: "center", 
    justifyContent: "center", 
    alignItems: "center", 
    cursor: "pointer",
  }, 
};


