// from the caller perspective during call pending 
export const START_CALL_FROM_LOCAL = "START_CALL_FROM_LOCAL"
export const REMOTE_ACCEPT_CALL = "REMOTE_ACCEPT_CALL"
export const REMOTE_REJECT_CALL = "REMOTE_REJECT_CALL"

// from the receiver perspective during call pending 
export const RECEIVE_REMOTE_CALL = "RECEIVE_REMOTE_CALL"
export const LOCAL_ACCEPT_CALL = "LOCAL_ACCEPT_CALL" 
export const LOCAL_REJECT_CALL = "LOCAL_REJECT_CALL" 


// error handling 
export const ERROR = "ERROR" 


// call hangup 
export const LOCAL_HANGUP_CALL = "LOCAL_HANGUP_CALL" 
export const REMOTE_HANGUP_CALL = "REMOTE_HANGUP_CALL"


export const REMOTE_INCOMING_CALL = "REMOTE_INCOMING_CALL"

