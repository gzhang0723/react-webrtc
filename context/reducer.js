/**
 *  call component state 
 *  
 * 
 * state {
 * RTCParam, 
 * RTCCall, 
 * RTCState 
 * }
 * 
 * 
 */


import {useReducer} from 'react';
import { 
    START_CALL_FROM_LOCAL,
    REMOTE_ACCEPT_CALL,
    REMOTE_REJECT_CALL,
    RECEIVE_REMOTE_CALL, 
    LOCAL_ACCEPT_CALL, 
    LOCAL_REJECT_CALL, 
    ERROR, 
    LOCAL_HANGUP_CALL,
    REMOTE_HANGUP_CALL,
    REMOTE_INCOMING_CALL
} from './actions';

const useCallStateRedicer= (initialState) => { 
    
    const callReducer = (state, action) => {
        console.log(`callReducer state is ${JSON.stringify(state)} and action is ${JSON.stringify(action)}`)
        let {alert} = state.CallParam.alert 
        switch(action.type){
            
            case START_CALL_FROM_LOCAL:
                // if press the start button, if slient mode, then it will start the call state
                // if call alert is enabled, then it will show the calling window 
                return {
                            ...state, 
                            RTCState: "StartCalling",
                            RTC_Call: action.payload.RTC_Call 
                        } 

            case REMOTE_INCOMING_CALL:
                console.log("state reducer REMOTE_INCOMING_CALL")
                return {
                            ...state, 
                            RTCState: "IncomingCall",
                        } 

            case REMOTE_ACCEPT_CALL:
                console.log("state reducer REMOTE_ACCEPT_CALL")
                return {
                            ...state, 
                            RTCState: "Accepted",
                        } 

            case REMOTE_REJECT_CALL:
            case ERROR: 
                return {
                            ...state, 
                            RTCState: "Initial"
                        } 

            case REMOTE_HANGUP_CALL:
            case LOCAL_HANGUP_CALL: 
                return {
                            ...state, 
                            RTCState: "Initial"
                        }
            default:
                return state
        }
    
    }
    
    console.log("callReducer intiial state ", initialState)
    const [callState, dispatchCallEvent] = useReducer(callReducer, initialState)
    
    return [callState, dispatchCallEvent] 

}

export default useCallStateRedicer