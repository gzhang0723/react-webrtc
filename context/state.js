//https://github.com/LloydJanseVanRensburg/Todo-List-Context-Project-/blob/master/src/context/TodoState.js

import * as React from 'react';
import { 
    START_CALL_FROM_LOCAL,
    REMOTE_ACCEPT_CALL,
    REMOTE_REJECT_CALL,
    RECEIVE_REMOTE_CALL, 
    LOCAL_ACCEPT_CALL, 
    LOCAL_REJECT_CALL, 
    ERROR, 
    LOCAL_HANGUP_CALL,
    REMOTE_HANGUP_CALL,
    REMOTE_INCOMING_CALL
} from './actions';

import CallContext from './context';
import useCallStateReducer from "./reducer";
import params from "./param";
import { Call } from "../AppRTCJS/call";
import useChat from "../components/useChat";

const CallStore = (props) => {
    console.log("CallStore props are ", props)
    const {alert, joinExisting, roomId} = props 
    const CallParam = {
        alert,          // whether or not to show call notification
        roomId
    }
    // callState initialState 
    const initalState = {
        CallParam, 
        RTCParam: params,
        RTCState: "Initial", 
    }
    const [callState, dispatchCallEvent] = useCallStateReducer(initalState)
    let RTC_Call = {} 

    //this is the call alerting component 
    const { messages, sendMessage } = useChat(roomId);

    //websocket call incoming event is not handled by UI so we dispatch here. 
    React.useEffect(()=>{
        if (!(messages.ownedByCurrentUser) && (messages.body == "Hello")) {
            dispatchCallEvent({
                type: REMOTE_INCOMING_CALL,
                payload: { }
            })
        }
    }, [messages])


    // start the RTC call when user press the start button 
    const startRTC = (isCaller=true) => {
            
            // RTC_Call handles peerconnection and websocket 
            RTC_Call = new Call(params)
            console.log("[webrtc context][state.js] Start RTC call ", RTC_Call);

            RTC_Call.params_.isInitiator = isCaller? true : false;   // whether or not this is the first caller 
            
            RTC_Call.onlocalstreamadded = (localStream) => { 
                RTC_Call.localStream = localStream
                dispatchCallEvent({
                    type: START_CALL_FROM_LOCAL,
                    payload: { RTC_Call }
                })
            }   
            
            RTC_Call.onremotestreamadded = (remoteStream) => { 
                RTC_Call.remoteStream = remoteStream
                dispatchCallEvent({
                    type: REMOTE_ACCEPT_CALL,
                    payload: { RTC_Call }
                })
            }
            
            RTC_Call.onremotehangup = () => { 
                RTC_Call && RTC_Call.hangup() 
                dispatchCallEvent({
                    type: REMOTE_HANGUP_CALL
                })
            }

            RTC_Call.start(roomId)

            if (alert && isCaller) sendMessage("Hello")  
    }
    

    const stopRTC = () => {   
        callState.RTC_Call.hangup() 
        dispatchCallEvent({type: LOCAL_HANGUP_CALL})
    }


    const callRemoteRejct = () => {
        console.log("[webrtc module caller side][state.js] local hangup received")
        dispatchCallEvent({type: REMOTE_REJECT_CALL})
    }


    // from receiver perspecive 
    // remote got the call notification and will add its stream 
    const callRemoteReceive = (localStream) => {
        dispatchCallEvent({
            type: RECEIVE_REMOTE_CALL, 
            payload: { localStream }})
    }

    // receiver accept the caller remotestream 
    const callLocalAccept = (remoteStream) => {
        dispatchCallEvent({
            type: LOCAL_ACCEPT_CALL, 
            payload: {remoteStream}})
    }

    const callLocalRejct = () => {
        dispatchCallEvent({type: LOCAL_REJECT_CALL})
    }

    const callError = () => {
        dispatchCallEvent({type: ERROR})
    }

    return ( 
        <CallContext.Provider
            value={{
                callState,
                // callLocalStart, 
                // callRemoteAccept, 
                callRemoteRejct,
                startRTC, 
                // callRemoteHangup,
                callRemoteReceive,
                callLocalAccept, 
                callLocalRejct, 
                stopRTC, 
                callError
            }}
        >
            {props.children}
        </CallContext.Provider> );
}
 
export default CallStore;