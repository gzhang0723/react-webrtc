export const params = {
  bypassJoinConfirmation: false,
  clientId: "39332334",
  errorMessages: [],
  header_message: "",
  iceServerTransports: "",
  iceServerUrl: "https://appr.tc/v1alpha/iceconfig?key=",
  includeLoopbackJs: "",
  isInitiator: false,
  isLoopback: false,
  mediaConstraints: {
    audio: false,
    video: {
      mandatory: {},
      optional: [
        {
          minWidth: 1280,
        },
        {
          minHeight: 720,
        },
      ],
    },
  },
  messages: [],
  offerOptions: {},
  peerConnectionConfig: {
    bundlePolicy: "max-bundle",
    iceServers: [
      {
        urls: ["stun:turn2.l.google.com"],
      },
    ],
    rtcpMuxPolicy: "require",
  },
  peerConnectionConstraints: {},
  roomServer: "https://sip.talkdesk.live:8089",
  versionInfo: {
    gitHash: "e5f7d3663738ba49eb91aea028b5cf3559c25051",
    time: "Tue Jun 22 14:25:03 2021 +0000",
    branch: "master",
  },
  warningMessages: [],
  wssUrl: "wss://sip.talkdesk.live:8089/ws",
  SOCKET_SERVER_URL: "http://localhost:9000", 
};

export default params;
