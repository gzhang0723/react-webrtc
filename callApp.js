
/*
 *
 *  PLEASE MINIMIZE changes to the library itself
 *  This module is for media stream only. The customized app logic or UI can be handled in different place
 *  The accompanying testing module is to demo how to use this module and for reference only
 *
 */

/**
 * components props
 *
 *
 * @param {Boolean} debug false will disable video div
 * @param {number} roomId logic seperation for video call groups
 * @param {Boolean} record should this component record the call
 * @param {func} onGetLocalStream local stream external handler
 * @param {func} onGetRemoteStream remote stream external handler
 * @param {Boolean} isStopRecoring the stop recording signaling from external
 * @param {callback->uuid, fileSize, duration} onRecordingReady callback for recording ready, will pass back the uuid of the recording file
 * @param {String} buttonView customized button label
 * @param {Boolean} joinExisting wether or not the starter of the room
 * @param {Boolean} startRTC start RTC externally
 * @param {text} buttonText start RTC externally
 * @param {Boolean} videoEnabled the video constraint enabled signaling from external
 * @param {String} videoPoster the video background url
 *
 *
 * Things can be customized:
 * 1. button view
 * 2. seperate streams to redux store; so button and streams can be seperated
 * 3. support multiple streams and peers [Todo]
 *
 */

 import * as React from 'react'
 import { useEffect, useRef, useState } from "react";
 import { Call } from "./AppRTCJS/call";
 import { v4 as uuidv4 } from "uuid";
 import { startRecording, stopRecording } from "./services/kurento";
 import { getMixStream, CatchAll } from "./services/mixer";
 //import params from "./context/param";
 import adapter from "webrtc-adapter";
 import { parseJSON } from "./AppRTCJS/util";
 import CallPendingComponent from "./components/CallPending";
 import CallAcceptedComponent from "./components/CallAccepted";
 import io from "socket.io-client";
 import axios from "axios";
 //import useCallStateRedicer, {START_CALL, REJECT_CALL, ACCEPT_CALL, HANGUP_CALL} from "./context/reducer";
 import CallButton from './components/CallButtons';
 
 import CallContext from "./context/context";
 import {useContext} from "react"
 
 const CallApp = (props) => {
   let {
     record,
     roomId,
     joinExisting,
     isStopRecoring,
     onRecordingReady,
     showDisplay,
     buttonText,
     onMixStreamDebug,
     videoEnabled,
     videoPoster,
     extStyle,
     callExtStatus,
     alert
   } = props;
   console.log("props are ", props);
 
   const {callState} = useContext(CallContext)
   //const [RTC_Call, setRTC_Call] = useState(null);
   const [localStream, setLocalStream] = useState(null);
   const [remoteStream, setRemoteStream] = useState(null);
   const [mixStream, setMixStream] = useState(null);
   const [uuid, setUUID] = useState(null);
 
   //console.log("params are ", params);
   console.log("WebRTC engine is: ", adapter.browserDetails.browser);
   console.log("WebRTC engine version is: ", adapter.browserDetails.version);
 
 
   // recording handler
  //  useEffect(() => {
  //    console.log("stopping recording......");
  //    stopRecording();
  //    if (isStopRecoring) {
  //      //TODO: optimize this. The aws s3 link need 2 seconds to have the recording ready. otherwise you will get 0 byte of file size with http head request method
  //      setTimeout(async () => {
  //        const response = await axios.get(
  //          `http://frank.dev.acquire.io:3001/api/replayMeta/${uuid}`
  //        );
  //        console.log(response);
  //        const fileSize = response.data.fileSize;
  //        const duration = response.data.duration;
  //        console.log(fileSize, duration);
  //        onRecordingReady(uuid, fileSize, duration);
  //      }, 2000);
  //    }
  //  }, [isStopRecoring]);
 
  //  useEffect(() => {
  //    if (record) {
  //      const _uuid = uuidv4();
  //      setUUID(_uuid);
  //      const _mixStream = getMixStream(localVideo.current.srcObject, [
  //        remoteStream,
  //      ]);
  //      setMixStream(_mixStream);
  //      onMixStreamDebug(_mixStream);
  //      _mixStream && startRecording(_mixStream, _uuid);
  //    }
  //  }, [remoteStream]);
 
  //  useEffect(() => {
  //    if (callExtStatus) dispatchCallEvent(START_CALL); 
  //      //setCallStatus("Pending");
  //  }, [callExtStatus]);

   // depends on callState, the UI will render accordingly 
   const renderCall = callState => {
     console.log("renderCall callState is ", callState); 
     switch (callState.RTCState) {

       case "Initial":
            return <CallButton 
                      roomId = {roomId}
                      joinExisting = {joinExisting}
                   />

       case "StartCalling":
            return <CallAcceptedComponent />

       case "IncomingCall": 
            return <CallPendingComponent
                  />

       case "Accepted":
            return <CallAcceptedComponent
                    videoPoster={videoPoster} 
                    />
                    
     }
   }
 
 
   return (
     <div>
        {renderCall(callState)}
     </div>
   );
 };
 
 export default CallApp;
 