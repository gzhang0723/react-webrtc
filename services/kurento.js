import param from "../context/param";

const kurento = require("kurento-client");
const {
  kmsCpuMemProfileLongPolling,
  stopLongPolling,
} = require("./kurentoServerManager");

const kurentoConfig = {
  ws_uri: "wss://frank.dev.acquire.io:8433/kurento",
  file_uri: "file:///tmp/recordings/",
};
const configuration = param.peerConnectionConfig;
// const configuration = {
//   iceServers: [
//     {
//       urls: [
//         "turn:turn4.acquire.io:443",
//         "turn:turn4.acquire.io:80",
//         // "turn:turn2.acquire.io:443",
//         // "turn:turn2.acquire.io:80",
//       ],
//       username: "test",
//       credential: "test",
//     },
//     {
//       urls: [
//         "stun:stun.l.google.com:19302",
//         "stun:stun1.l.google.com:19302",
//         "stun:stun2.l.google.com:19302",
//       ],
//     },
//   ],
// };
const elements = [
  {
    type: "RecorderEndpoint",
    params: {},
  },
  { type: "WebRtcEndpoint", params: {} },
];
let pipeline = null;
let _webRtcEndpoint = null;
let _recorderEndpoint = null;
const candidatesQueue = [];
let kmsCpuMemProfileLongPollingID = null;
/**
 * Stop recording pipeline
 */
export function stopRecording() {
  if (pipeline) {
    console.log("Stopping recording ...");
    _recorderEndpoint.stopAndWait((error) => {
      if (error) console.log(error);
      console.log("releasing pipeline ...");
      pipeline.release();
      stopLongPolling(kmsCpuMemProfileLongPollingID);
    });
  }
}

function dignosisCaller(pc) {
  pc.onicecandidateerror = (e) => {
    console.log(e);
  };
}

function dignosisCallee(endpoint) {
  endpoint.on("IceComponentStateChange", (e) => {
    console.log("******************************** IceComponentStateChange");
    console.log(e);
  });
}
/**
 *
 * @param {MediaStream} mediaStream the stream that is going to be pushed into peer connection
 * @param {String} uuid The uuid for the recording file
 *
 * Start the process
 * This function is going to setup peers for both side
 *
 * step 1: setup kurento side (pipeline, elements, listeners for webRtcEndpoint, connecting elements)
 * step 2: setup browser side (peer, listeners for peer)
 * step 3: proccess offer/answer exchange
 * step 4: ? cache icecandidate before answer arrived.
 * step 5: enable recording on recorderEndpoint
 *
 */

export async function startRecording(mediaStream, uuid) {
  //setup the uuid
  elements.find((element) => element.type === "RecorderEndpoint").params.uri =
    kurentoConfig.file_uri + uuid + ".webm";

  // if (isAudioOnly)
  //   elements.find(
  //     (element) => element.type === "RecorderEndpoint"
  //   ).params.mediaProfile = "WEBM_AUDIO_ONLY";

  console.log(elements);
  try {
    console.log("******************************** starting recording ...");
    // setup kurento side
    const kurentoClient = await kurento(kurentoConfig.ws_uri);
    pipeline = kurentoClient.create("MediaPipeline");

    console.log("******************************** Got MediaPipeline");
    console.log(pipeline);
    const [recorderEndpoint, webRtcEndpoint] = await createMediaElements(
      pipeline,
      elements
    );
    webRtcEndpoint.on("OnIceCandidate", (event) => {
      const candidate = kurento.getComplexType("IceCandidate")(event.candidate);
      console.log(
        "******************************** webRtcEndpoint generated candidate"
      );
      console.log(candidate);
      onCalleeIceCandidate(candidate, caller);
    });
    await webRtcEndpoint.connect(recorderEndpoint);

    // browser side
    const caller = new RTCPeerConnection(configuration);
    dignosisCallee(webRtcEndpoint);

    caller.onicecandidate = onCallerIceCandidate;

    dignosisCaller(caller);

    addMediaTracks(mediaStream, caller);

    // offer/answer process
    console.log("******************************** Generating Offer...");
    const sdpOffer = await caller.createOffer();
    await caller.setLocalDescription(sdpOffer);

    const sdpAnswer = await webRtcEndpoint.processOffer(sdpOffer.sdp);
    console.log("******************************** Got Answer");
    console.log(sdpAnswer);
    await caller.setRemoteDescription(
      new RTCSessionDescription({ type: "answer", sdp: sdpAnswer })
    );

    // offer/answer process has finished, let the globle webRtcEndpoint point to this local webRtcEndpoint
    _webRtcEndpoint = webRtcEndpoint;
    _recorderEndpoint = recorderEndpoint;
    await webRtcEndpoint.gatherCandidates();

    await configureRecording(webRtcEndpoint, recorderEndpoint);
    console.log("Recording...");
    // await recorderEndpoint.record();

    //get metrics from kms
    kmsCpuMemProfileLongPollingID = await kmsCpuMemProfileLongPolling(1000);
    console.log(kmsCpuMemProfileLongPollingID);
  } catch (error) {
    console.log(error);
    pipeline.release();
  }
}

/**
 * has to be called after recording has finished
 * @returns
 */
export function getUUID() {
  return elements
    .find((element) => element.type === "RecorderEndpoint")
    .params.uri.split("/")
    .pop();
}
/**
 * Turn on recording if getting stream flow into the pipeline and endpoints
 * @param {*} webRtcEndpoint
 * @param {*} recorderEndpoint
 */
async function configureRecording(webRtcEndpoint, recorderEndpoint) {
  const pipeline = await recorderEndpoint.getMediaPipeline();

  let video = false;
  let audio = false;

  webRtcEndpoint.on("MediaFlowOutStateChange", function (event) {
    console.log(
      "******************************** MediaFlowOutStateChange webRtcEndpoint"
    );
    console.log(event);
    if (event.mediaType === "AUDIO") {
      audio = true;
    }
    if (event.mediaType === "VIDEO") {
      video = true;
    }
    if (event.state === "FLOWING" && video && audio) {
      recorderEndpoint.record((error) => {
        if (error) {
          console.error("error when trying to record");
          pipeline.release();
        }
        console.log("Recording...");
      });
    }

    // if (event.state === "FLOWING") {
    //   recorderEndpoint.record((error) => {
    //     if (error) {
    //       console.error("error when trying to record");
    //       pipeline.release();
    //     }
    //     console.log("Recording...");
    //   });
    // }
  });

  recorderEndpoint.on("MediaFlowInStateChange", function (event) {
    //this is only triggered when the record() method has been called
    console.log(
      "******************************** MediaFlowInStateChange recorderEndpoint"
    );
    console.log(event);
    if (event.state !== "FLOWING") {
      recorderEndpoint.stopAndWait((error) => {
        console.log(error);
        // audio = false;
        // video = false;
        console.log("releasing pipeline ...");
        pipeline.release();
      });
    }
  });
}

/**
 * push media track into RTCPeerConnection
 * @param {*} mediaStream
 * @param {*} peer in this case should be the caller
 */
function addMediaTracks(mediaStream, peer) {
  const videoTracks = mediaStream.getVideoTracks();
  const audioTracks = mediaStream.getAudioTracks();
  if (videoTracks.length > 0) {
    console.log(`Using video device: ${videoTracks[0].label}`);
    console.log(videoTracks);
  }
  if (audioTracks.length > 0) {
    console.log(`Using audio device: ${audioTracks[0].label}`);
    console.log(audioTracks);
  }
  mediaStream.getTracks().forEach((track) => peer.addTrack(track, mediaStream));
}

/**
 * When caller generates an iceCandidate, pass it to  callee and callee add it.
 * TODO: need to make sure candidates are added after answer/offer exchanged.
 * @param candidate
 * @param callee the webRtcEndpoint at kurento side
 */
/**
 * callback function of RTCPeerConnection.onicecandidate
 * @param {*} e
 * @returns
 */
function onCallerIceCandidate(e) {
  const candidate = e && e.candidate;
  console.log(
    "caller generated an ice candidate, onCallerIceCandidate method called"
  );
  //some how, the candidate might be null
  if (!candidate) return;

  const iceCandidate = kurento.getComplexType("IceCandidate")(candidate);
  iceCandidate.candidate = iceCandidate.candidate.toString();
  if (_webRtcEndpoint) {
    console.log(
      "******************************** finished answer/offer exchanged, add iceCandidates ..."
    );
    while (candidatesQueue.length) {
      console.log(
        "******************************** add cached candidate to webrtcEndpoint"
      );
      const candidateInQueue = candidatesQueue.shift();
      _webRtcEndpoint.addIceCandidate(candidateInQueue);
    }

    _webRtcEndpoint.addIceCandidate(candidate);
  } else {
    console.log(
      "******************************** not yet finished answer/offer exchanged, queueing iceCandidate ..."
    );
    candidatesQueue.push(candidate);
  }
}

/**
 * When callee generates an iceCandidate, pass it to caller, and caller add it.
 * @param _candidate from webrtcendpoint in kms
 * @param caller the brower side peer
 */
function onCalleeIceCandidate(candidate, caller) {
  console.log(
    "callee generated an ice candidate, onCalleeIceCandidate method called"
  );
  const rtcIceCandidate = new RTCIceCandidate(candidate);
  // console.log(caller.signalingState);
  caller.addIceCandidate(rtcIceCandidate);
}

/**
 * create media elements for the pipeline
 * @param {*} pipeline the pipeline object that elements going to attach to
 * @returns Promise<array> follow the same order as the elements in the configuration
 */
function createMediaElements(pipeline, elements) {
  return new Promise(function (resolve, reject) {
    pipeline.create(elements, function (error, elements) {
      if (error) reject(error);

      resolve(elements);
    });
  });
}

export default {
  stopRecording,
  startRecording,
  getUUID,
};
