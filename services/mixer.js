    // mixer engine 
    import MultiStreamsMixer from "multistreamsmixer";

    // added try-cache decorator 
    const CatchAll = (func1, param) => {
            try {
                return func1(...param)
            } catch (error) {
                console.log("Error is:", error.message)
            }
    }

    // **** utilities for debugging the mixer;; 
    // **** mixer output will be recorded in kurento
    const getMixStreamRaw = (backgroundStream, otherStreams) => {
        if (backgroundStream && otherStreams) {

            backgroundStream.fullcanvas = true;
            backgroundStream.width = window.screen.width; 
            backgroundStream.height = window.screen.height; 
            
            // make sure each remote stream has right width/height 
            let ratio = parseInt( 100 / ( otherStreams.length +1 )) + 1 
            otherStreams.forEach(remoteStream => {
                remoteStream.width = parseInt((ratio / 100) * backgroundStream.width);
                remoteStream.height = parseInt((ratio / 100) * backgroundStream.height);
            })
            
            let mixer = new MultiStreamsMixer([backgroundStream, ...otherStreams])

            mixer.frameInterval = 1;
            mixer.startDrawingFrames();

            return mixer.getMixedStream()

        } else console.log("[getMixStream] inputs streams are empty")
    }

    export const getMixStream = (backgroundStream, otherStreams) => CatchAll(getMixStreamRaw, [backgroundStream, otherStreams])

    export default {
        getMixStream,
        CatchAll,
    };