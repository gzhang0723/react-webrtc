const kurento = require("kurento-client");
const kurentoConfig = {
  ws_uri: "wss://frank.dev.acquire.io:8433/kurento",
  file_uri: "file:///tmp/recordings/",
};

async function getServerManager() {
  const kurentoClient = await kurento(kurentoConfig.ws_uri);
  console.log("******************************** Got kurentoClient");
  const serverManager = await kurentoClient.getServerManager();
  console.log("******************************** Got serverManager");
  return serverManager;
}

/**
 * Get the used cpu and memory metrics from kms server.
 * @param {*} serverManager the serverManager object
 * @param {*} interval Time to measure the average CPU usage, in milliseconds. normally from 1000 to 10000
 */
async function getUsedCpuAndMemoryMetrics(serverManager, interval) {
  try {
    const usedCPU = await serverManager.getUsedCpu(interval);
    const usedMemory = await serverManager.getUsedMemory();
    const usedMemoryInMB = usedMemory * 0.001024;
    console.log(usedCPU.toFixed(2) + " % CPU has been used");
    console.log(usedMemoryInMB.toFixed(2) + " mb Memory has been used");
  } catch (error) {
    console.error(error);
  }
}

/**
 * get cpu and memory metrics periodically
 * @param {*} interval Time to measure the average CPU usage, in milliseconds. It is also the interval time.
 * @returns
 */
export async function kmsCpuMemProfileLongPolling(interval) {
  const serverManager = await getServerManager();

  const intervalID = setInterval(() => {
    getUsedCpuAndMemoryMetrics(serverManager, interval);
  }, interval);

  return intervalID;
}

/**
 * stop the long polling interval
 * @param {*} intervalID the intervalID that needs to be stopped.
 */
export async function stopLongPolling(intervalID) {
  clearInterval(intervalID);
}

export default {
  kmsCpuMemProfileLongPolling,
  stopLongPolling,
};
